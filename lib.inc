section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
        cmp byte [rdi + rax], 0
        je .end ;
        inc rax
        jmp .counter
    .end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ; string_length change rsi content, so we have to store it before call
    call string_length
    pop rdi ; store rdi value as input of function 
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret
    
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret
; why in this code, we have to push rdi into stack ?
; because rsi just accept pointer as argument
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    push rdx
    mov rbx, 10
    push 0x00 ; push end of string to bottom of stack
    push rdi ; store rdi into stack 
    cmp rdi, 0x2D; check if arg is negative
    je .sign ; print '-' sign
    pop rdi ; retake rdi from stack
    mov rax, rdi
    .loop:
    	xor rdx, rdx
    	div rbx
    	add rdx, 0x30
    	push rdx
    	cmp rax, rbx
    	jae .loop
    	add rax, 0x30
        cmp rax, 0x30
        je .next
    	push rax
    	xor rax, rax
    .next:
    	pop rdi
    	test rdi, rdi
    	je .eof
    	call print_char
    	jmp .next
    .eof:
    	pop rdx 
    	ret
    .sign: 
    	mov rdi, 0x2D
    	pop rdx
    	jmp print_char

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rbx, rdi
    test rdi, rdi
    jns .end
    mov rdi, '-'
    call print_char
    mov rdi, rbx
    neg rdi
    .end:
    	jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
   xor rcx, rcx 
   .loop:
   mov al, [rdi + rcx]
   mov dl, [rsi + rcx]
   cmp al, dl
   jne .neq
   inc rcx
   cmp al, 0
   je .eq
   jmp .loop
    .eq:
    mov rax, 1
    ret
    .neq:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    xor rax, rax
    syscall
    pop rax
    ret
    
section .data
word_buffer times 256 db 0

section .text

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r10, word_buffer
    .spaces:
    call read_char
    test rax, rax
    jz .en
    cmp rax, 0x20
    jle .spaces
    
    .next:
    mov [r10], rax
    inc r10
    call read_char
    cmp rax, 32
    jg .next
    
    .en:
    mov rdi, word_buffer
    call string_length
    cmp rax, 19
    ja .end
    mov rdx, rax
    mov rax, word_buffer
    ret
    .end:
    xor rdx, rdx
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rcx, rcx
xor rax, rax
xor r8, r8
mov r10, 10
.next:
cmp byte[rdi + rcx], '0'
jb .end
cmp byte[rdi + rcx], '9'
ja .end
mul r10
mov r8b, [rdi + rcx]
sub r8b, '0'
add rax, r8
inc rcx
jmp .next
.end:
mov rdx, rcx
ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;rdi, rsi, rdx
string_copy:
	xor rcx, rcx
	mov r10, rdi
	call string_length
	cmp rax, 19
	ja .end
	mov rdi, r10
	mov cl, [rdi]
	mov [rsi], cl
	inc rdi
	inc rsi
	test rcx, rcx
	jnz string_copy
	
	.end:
	ret
